package aiso.sim.hardware;

import aiso.sim.instructions.Instruction;
import aiso.sim.os.Context;
import aiso.sim.os.Logger;
import aiso.sim.os.OperatingSystem;
import aiso.sim.os.syscalls.SysCallNumber;

public class SimpleCPUCore extends CPUCore {
	
	/**
	 * Number of registers included in each CPU core
	 */
	private final static int NUMBER_OF_REGISTERS = 20;
	
	/**
	 * The core's registers
	 */
	private final Object[] registers = new Object[NUMBER_OF_REGISTERS];
	
	/**
	 * CPU clocks that the instruction currently in execution will still take on the core
	 * before concluding
	 */
	private int instructionRunningTime;


	@Override
	public synchronized void load(Context context) {
		super.load(context);
//		instructionRunningTime = 0; //provavel erro no caso de ser um exit a var fica a negativo
	}

	/**
	 * The fetch-decode-execute loop
	 */
	@Override
	public synchronized void tick() {
		if (context != null) {
			if (instructionRunningTime == 0) {
				try {
					// Fetch
					Instruction inst = fetch();
					// Decode
					this.instructionRunningTime = inst.getCPUClocks();
					// Execute
					inst.run(this);
//					throw new Exception(); TODO To test the syscall of the cpu to the OS 
				} catch (Exception e) {
					context = null;
					//send exception to the OS
					Logger.internalError("[SIMPLE_CPU_CORE_TICK] EXCEPTION");
					try {
						// TODO set the error type has the second argument
						setRegisters(0, SysCallNumber.CPU_EXCEPTION); 
					} catch (InvalidRegisterException e1) {
						// Do nothing, the registers are valid!
					}
					handleInterrupt(Interrupt.SYSCALL);
					
				}
			}
			instructionRunningTime--;
		}
	}

	@Override
	public String getDescription() {
		return "Simple CPU core";
	}

	@Override
	public void handleInterrupt(Interrupt interrupt) {
		OperatingSystem.getInstance().getInterruptVector()[interrupt.ordinal()].handle(this);
	}

	@Override
	public void setRegisters(int register, Object... values) throws InvalidRegisterException {
		for (Object value : values) {
			if (register >= NUMBER_OF_REGISTERS)
				throw new InvalidRegisterException(register);
			this.registers[register++] = value;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> T getRegister(int i) throws InvalidRegisterException {
		if (i >= NUMBER_OF_REGISTERS)
			throw new InvalidRegisterException(i);
		return (T) registers[i];
	}

	@Override
	public Object[] getRegisters() {
		return this.registers;
	}

}
