package aiso.sim.os;

import java.util.PriorityQueue;

import aiso.sim.Configuration;
import aiso.sim.hardware.CPUCore;


/**
 * @author Luis Silva Nr. 44890
 * @author Ricardo Gaspar Nr. 42038
 */
public class ProcessControlBlockStructure {
	
	//private static final int INITIALCAPACITY = 10;
	
	private CPUCore[] cpuCores = Configuration.cpuCores;
	
	/**
	 * A queue of READY processes
	 */
	private  PriorityQueue<ProcessControlBlock> readyProcessQueue;
	
	/**
	 * A queue of BLOCKED processes
	 */
	//private  PriorityQueue<ProcessControlBlock> blockedProcessQueue;
	
	public ProcessControlBlockStructure(){
		//Comparator<String> comparator = new StringLengthComparator();
		//readyProcessQueue = new PriorityQueue<>(INITIALCAPACITY, comparator);
		readyProcessQueue = new PriorityQueue<>();
	}
	
	/**
	 * Enqueues a READY Process
	 * @param process - The process
	 */
	public void enqueueReadyProcess(ProcessControlBlock process){
		readyProcessQueue.add(process);
	}

	/**
	 * Gets the next READY process to be executed
	 * 
	 * @return The process
	 * @throws EmptyQueueException
	 */
	public ProcessControlBlock dequeueReadyProcess() throws EmptyQueueException{
		if(readyProcessQueue.isEmpty()){
			throw new EmptyQueueException();
		}
		return readyProcessQueue.poll();
	}
	
	/**
	 * Return the first free core available.
	 * 
	 * @return - The core
	 * @throws NoCoreAvailableException 
	 */
	public CPUCore getFreeCore() throws NoCoreAvailableException{
		for (int i = 0; i < cpuCores.length; i++) {
			if(cpuCores[i].getContext() != null){
				Logger.info("[getFreeCore] Core "+ i+ " is free!");
				return cpuCores[i];
			}
		}
		throw new NoCoreAvailableException();
	}
	
}
