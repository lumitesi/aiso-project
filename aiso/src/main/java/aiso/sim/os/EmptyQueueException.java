package aiso.sim.os;


/**
 * @author Luis Silva Nr. 44890
 * @author Ricardo Gaspar Nr. 42038
 */
public class EmptyQueueException extends Exception {
	
	/**
	 * Default serial version ID
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor
	 */
	public EmptyQueueException() {
		super("Empty Queue");
	}

}
