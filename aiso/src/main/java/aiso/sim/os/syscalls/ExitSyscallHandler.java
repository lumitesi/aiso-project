package aiso.sim.os.syscalls;

import aiso.sim.hardware.CPUCore;
import aiso.sim.os.EmptyQueueException;
import aiso.sim.os.Logger;
import aiso.sim.os.NoCoreAvailableException;
import aiso.sim.os.OperatingSystem;
import aiso.sim.os.OperatingSystemClass;
import aiso.sim.os.ProcessControlBlockStructure;

/**
 * @author Luis Silva Nr. 44890
 * @author Ricardo Gaspar Nr. 42038
 */
public class ExitSyscallHandler implements SysCallHandler {

	@Override
	public void handle(CPUCore core) {
		Logger.info("[EXIT_SYSCALL] begin.");
		
		// clears the context of the core
		core.load(null);
		OperatingSystemClass os = (OperatingSystemClass) OperatingSystem
				.getInstance();
		ProcessControlBlockStructure scheduler = os.getScheduler();
		try {

			// Verify if the core is free, in case isn't gets one that is
			if (core.getContext() != null) {
				core = scheduler.getFreeCore();
			}

			// load a context from the ready queue
			core.load(scheduler.dequeueReadyProcess().getContext());
		} catch (EmptyQueueException e) {
			Logger.internalError("[EXIT_SYSCALL] There is no ready PCB in the queue.");
		} catch (NoCoreAvailableException e) {
			Logger.internalError("[EXIT_SYSCALL] There is no free Core.");
		}

		Logger.info("[EXIT_SYSCALL] end.");
	}

}
