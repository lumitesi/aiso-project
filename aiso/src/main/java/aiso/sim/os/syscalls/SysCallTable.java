package aiso.sim.os.syscalls;

import java.util.HashMap;
import java.util.Map;

public class SysCallTable {
	
	static final Map<SysCallNumber, SysCallHandler> table = 
			new HashMap<SysCallNumber, SysCallHandler>();
			
	static {
		table.put (SysCallNumber.SOME_SYSCALL_0, new DummySyscallHandler()); //TODO for test only
		table.put (SysCallNumber.SOME_SYSCALL_1, new DummySyscallHandler());//TODO for test only
		table.put (SysCallNumber.YELD, new YeldSyscallHandler());
		table.put (SysCallNumber.LOAD_PROGRAM, new LoadSyscallHandler());
		table.put (SysCallNumber.EXIT, new ExitSyscallHandler());
		table.put(SysCallNumber.CPU_EXCEPTION, new CPUSyscallHandler());
	}
	
	public static SysCallHandler getHandler(SysCallNumber number) {
		return table.get(number);
	}
}
