package aiso.sim.os.syscalls;

import aiso.sim.hardware.CPUCore;
import aiso.sim.os.EmptyQueueException;
import aiso.sim.os.Logger;
import aiso.sim.os.NoCoreAvailableException;
import aiso.sim.os.OperatingSystem;
import aiso.sim.os.OperatingSystemClass;
import aiso.sim.os.ProcessControlBlock;
import aiso.sim.os.ProcessControlBlockStructure;

/**
 * @author Luis Silva Nr. 44890
 * @author Ricardo Gaspar Nr. 42038
 */
public class YeldSyscallHandler implements SysCallHandler {

	@Override
	public void handle(CPUCore core) {
		Logger.info("[YELD_SYSCALL] begin.");

		OperatingSystemClass os = (OperatingSystemClass) OperatingSystem
				.getInstance();
		ProcessControlBlockStructure scheduler = os.getScheduler();
		// Save the process context, and enqueue it's PCB to the ready queue
		scheduler
				.enqueueReadyProcess(new ProcessControlBlock(core.getContext()));
		
		// After saving the context that was running the core is free
		core.load(null);

		try {
			
			// Verify if the core is free, in case isn't gets one that is
			if(core.getContext() != null){
				core = scheduler.getFreeCore();
			}
			
			// Get a new PCB from the ready queue and load it to the CPU core
			core.load(scheduler.dequeueReadyProcess().getContext());
			
		} catch (EmptyQueueException e) {
			Logger.internalError("[YELD_SYSCALL] There is no ready PCB in the queue.");
//			e.printStackTrace();
		} catch (NoCoreAvailableException e) {
			Logger.internalError("[YELD_SYSCALL] There is no free Core.");
//			e.printStackTrace();
		}
		Logger.info("[YELD_SYSCALL] end.");
	}

}
