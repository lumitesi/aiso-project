package aiso.sim.os.syscalls;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

import aiso.sim.hardware.CPUCore;
import aiso.sim.hardware.InvalidRegisterException;
import aiso.sim.os.EmptyQueueException;
import aiso.sim.os.Logger;
import aiso.sim.os.NoCoreAvailableException;
import aiso.sim.os.OperatingSystem;
import aiso.sim.os.OperatingSystemClass;
import aiso.sim.os.ProcessControlBlock;
import aiso.sim.os.ProcessControlBlockStructure;
import aiso.sim.parser.ParseException;
import aiso.sim.parser.Parser;

/**
 * @author Luis Silva Nr. 44890
 * @author Ricardo Gaspar Nr. 42038
 */
public class LoadSyscallHandler implements SysCallHandler {

	@Override
	public void handle(CPUCore core) {
		Logger.info("[LOAD_SYSCALL] begin.");

		OperatingSystemClass os = ((OperatingSystemClass) OperatingSystem
				.getInstance());
		ProcessControlBlockStructure scheduler = os.getScheduler();
		String filename;
		Parser parser = null;
		ProcessControlBlock pcb = null;

		try {
			// Load the program file which name is stored in the register 1
			filename = core.getRegister(1);
			parser = new Parser(new FileInputStream(filename));

			int newPid = os.generatePID();
			pcb = new ProcessControlBlock(newPid, parser.Program());

			// Add to ready queue
			scheduler.enqueueReadyProcess(pcb);

			// Verify if the core is free, in case isn't gets one that is
			if (core.getContext() != null) {
				core = scheduler.getFreeCore();
			}
			// Get a process from the ready queue
			core.load(scheduler.dequeueReadyProcess().getContext());

		} catch (InvalidRegisterException e) {
			Logger.internalError("[LOAD_SYSCALL] Either the filename in the register 1 is not valid or the specified register does not exist.");
			// e.printStackTrace();
		} catch (FileNotFoundException e) {
			Logger.internalError("[LOAD_SYSCALL] The specified program file was not found.");
			// e.printStackTrace();
		} catch (ParseException e) {
			Logger.internalError("[LOAD_SYSCALL] The specified program file could not be parsed. Please check for errors.");
			// e.printStackTrace();
		} catch (EmptyQueueException e) {
			Logger.internalError("[LOAD_SYSCALL] There is no ready PCB in the queue.");
			// e.printStackTrace();
		} catch (NoCoreAvailableException e) {
			Logger.internalError("[LOAD_SYSCALL] There is no free Core.");
			// e.printStackTrace();
		}

		Logger.info("[LOAD_SYSCALL] end.");
	}

}
