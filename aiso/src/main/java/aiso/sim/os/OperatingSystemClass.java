/**
 * 
 */
package aiso.sim.os;

import aiso.sim.Configuration;
import aiso.sim.console.Console;

/**
 * @author Luis Silva Nr. 44890
 * @author Ricardo Gaspar Nr. 42038
 */
public class OperatingSystemClass extends OperatingSystem {

	protected InterruptHandler[] interruptVector;
	protected ProcessControlBlockStructure scheduler;
	protected static int pidCounter = 100;

	/**
	 * Operating System Class. OS implementation.
	 */
	public OperatingSystemClass() {
	}

	@Override
	public void load() {
		this.loadWithoutConsole();
		// Launch Console
		System.out.println("Initializing Console...");
		Console console = new Console();
		console.run();

	}

	public void loadWithoutConsole() {
		// List all hardware
		System.out.println("Listing hardware devices ...");
		System.out.println(Configuration.numberOfCPUCores + " CPU cores.");
		for (int i = 0; i < Configuration.cpuCores.length; i++) {
			System.out.println("Core " + i + " "
					+ Configuration.cpuCores[i].getDescription());
		}
		System.out.println("Clockable devices ...");
		for (int i = 0; i < Configuration.devices.length; i++) {
			System.out.println("Device " + i + " "
					+ Configuration.devices[i].getDescription());
		}

		// Initialize OS data structures

		System.out.println("Initializing Scheduler...");
		scheduler = new ProcessControlBlockStructure();
		System.out.println("Initializing Interrupt Handler table...");
		interruptVector = new InterruptHandler[] { new SysCallInterruptHandler() };
	}

	@Override
	public InterruptHandler[] getInterruptVector() {

		return interruptVector;
	}

	/**
	 * Generate a new PID. Increments the PID after the returned number.
	 * 
	 * @return a new PID.
	 */
	public int generatePID() {
		return pidCounter++;
	}

	public ProcessControlBlockStructure getScheduler() {
		return scheduler;
	}

}
