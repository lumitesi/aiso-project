/**
 * 
 */
package aiso.sim.os;

import aiso.sim.Program;

/**
 * Notion of process. The Process Control Block stores all the data related to
 * the process, so that the OS can manage its resources and permissions.
 * 
 * @author Luis Silva Nr. 44890
 * @author Ricardo Gaspar Nr. 42038
 */
public class ProcessControlBlock {

	private int pID;
	private Context pContext;
	private Object[] registers;

	/**
	 * PCB Contructor allows to create a PCB given a Process ID (PID) and a
	 * Program file.
	 * 
	 * @param pid
	 *            Process Identifier.
	 * @param prog
	 *            Program file.
	 */
	public ProcessControlBlock(int pid, Program prog) {
		this.pID = pid;
		this.pContext = new Context(pID, prog);
	}

	/**
	 * PCB Contructor allows to create a PCB based on the program's context.
	 * This is the case when the program is enqueued again to the scheduler's
	 * ready queue.
	 * 
	 * @param processContext
	 *            Context of a program.
	 */
	public ProcessControlBlock(Context processContext) {
		this.pID = processContext.getPID();
		this.pContext = processContext;
	}

	/**
	 * Get the process context.
	 * 
	 * @return the context.
	 */
	public Context getContext() {
		return pContext;
	}
}
