package aiso.sim;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import org.junit.Test;

import aiso.sim.parser.ParseException;
import aiso.sim.parser.Parser;

public abstract class BaseAISOTest {

	protected static final String testFolder = "."+File.separator+"aiso"+File.separator+"src" + File.separator + 
			"test" + File.separator + 
			"resources" + File.separator;
//	protected static final String testFolder = "src" + File.separator + 
//			"test" + File.separator + 
//			"resources" + File.separator;
	
	public Parser getParser() throws FileNotFoundException {
		return new Parser(new FileInputStream(testFolder + getAISOFileName()));
	}
	protected Program compile() throws FileNotFoundException, ParseException {
		return new Parser(new FileInputStream(testFolder + getAISOFileName())).Program();	
	}
	
	
	protected abstract String getAISOFileName();
	
	@Test
	public abstract void test();
	
	
	protected String getAbsoluteFileName() {
		return testFolder + getAISOFileName();
	}
}
