package aiso.sim.instructions;

import static org.junit.Assert.*;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import sun.misc.Signal;
import aiso.sim.BaseAISOTest;
import aiso.sim.Configuration;
import aiso.sim.Program;
import aiso.sim.console.ExecCommand;
import aiso.sim.hardware.CPUCore;
import aiso.sim.hardware.Clock;
import aiso.sim.hardware.Interrupt;
import aiso.sim.hardware.SimpleCPUCore;
import aiso.sim.os.Context;
import aiso.sim.os.OperatingSystemClass;
import aiso.sim.os.syscalls.SysCallNumber;
import aiso.sim.parser.ParseException;

public class RunProgramTest extends BaseAISOTest {

	@Override
	protected String getAISOFileName() {
		return "ex1.aiso";
	}

	@Test
	public void test() {

		new Clock().start();
		OperatingSystemClass os = (OperatingSystemClass) OperatingSystemClass
				.getInstance();
		os.loadWithoutConsole();
		List<String> arguments = new ArrayList<>();
		arguments.add(getAbsoluteFileName());

		// new ExecCommand().run(new ArrayList<String>() {{
		// add(getAbsoluteFileName()); }});
		try {
			execCommandRun(arguments);
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}

	@Test
	public void test2() {

		new Clock().start();
		OperatingSystemClass os = (OperatingSystemClass) OperatingSystemClass
				.getInstance();
		os.loadWithoutConsole();
		List<String> arguments = new ArrayList<>();
		arguments.add(getAbsoluteFileName());

		CPUCore core = new SimpleCPUCore();
		Program prog = null;
		try {
			prog = compile();
		} catch (FileNotFoundException | ParseException e) {
			fail();
		}
		

	}

	/**
	 * This is a cop of the ExecCommand run method, but it throws an exception.
	 * 
	 * @param arguments
	 * @throws Exception
	 */
	private void execCommandRun(List<String> arguments) throws Exception {
		// For simplicity's sake we are assuming that the console is
		// always run by CPU core 0, and thus the exec syscall commands
		// are always targeted at that same core
		Configuration.cpuCores[0].setRegisters(0, SysCallNumber.LOAD_PROGRAM,
				arguments.get(0));
		Configuration.cpuCores[0].handleInterrupt(Interrupt.SYSCALL);

	}

}
