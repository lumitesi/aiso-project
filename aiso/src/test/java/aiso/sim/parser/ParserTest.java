package aiso.sim.parser;

import static org.junit.Assert.*;

import org.junit.Test;

import aiso.sim.BaseAISOTest;
import aiso.sim.hardware.CPUCore;
import aiso.sim.hardware.SimpleCPUCore;
import aiso.sim.instructions.Instruction;
import aiso.sim.instructions.SysCall;
import aiso.sim.os.syscalls.SysCallNumber;

import java.util.Iterator;

public class ParserTest extends BaseAISOTest {

	@Test
	public void test() {
		try {
			CPUCore cpuCore = new SimpleCPUCore();
			Iterator<Instruction> it = compile().iterator();
			Instruction inst = null;
			while (it.hasNext()) {
				inst = it.next();
				System.out.println("INSTRUCTION: "+ inst.toString());
//				Instruction invalid_instruction = new SysCall(SysCallNumber.SOME_SYSCALL_0, arguments) 
//				.run(cpuCore);
//				cpuCore.setRegisters(register, values);
				//inst.run(cpuCore);
			}

			assertTrue(true);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			fail();
		}

	}

	@Override
	protected String getAISOFileName() {
		 return "simple.aiso";

	}

}
